# resume

## Caution

This resume is used for CTF only.
I don't garantee the correctness of the contents.

## Career

- Sales
- Sales trainer
- Network Engineer

## Speciality

- TCP/IP
    - CCNP(Expired)
    - JNCIA-Junos(Expired)
    - IPA Network Specialist
- Security
    - IPA Security Specialist
    - Tokumaru Basic Exam
- AWS
    - AWS Solution Architect Associate
